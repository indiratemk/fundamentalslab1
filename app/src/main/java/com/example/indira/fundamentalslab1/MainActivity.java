package com.example.indira.fundamentalslab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final String EXTRA_COUNT = "com.example.indira.fundamentals1.extra.COUNT";

    private int mCount = 0;
    private TextView mShowCount;
    private Button mResetButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setContentView(R.layout.activity_main);
        mShowCount = findViewById(R.id.show_count);
        mResetButton = findViewById(R.id.button_zero);

        Log.e(TAG, "Error");
        Log.d(TAG, "Debug");
        Log.i(TAG, "Info");
        Log.v(TAG, "Verbose");
        Log.w(TAG, "Warn");
    }

    public void showToast(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(EXTRA_COUNT, mCount);
        startActivity(intent);
    }

    public void countUp(View view) {
        mResetButton.setBackgroundColor(getResources().getColor((R.color.pink)));
        ++mCount;
        if (mCount % 2 == 0) {
            view.setBackgroundColor(getResources().getColor((R.color.green)));
        } else {
            view.setBackgroundColor(getResources().getColor((R.color.colorPrimary)));
        }
        if (mShowCount != null)
            mShowCount.setText(Integer.toString(mCount));
    }

    public void reset(View view) {
        view.setBackgroundColor(getResources().getColor(R.color.gray));
        mCount = 0;
        if (mShowCount != null)
            mShowCount.setText(Integer.toString(mCount));
    }
}
