package com.example.indira.fundamentalslab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    private TextView mMessageTextView;
    private TextView mCountTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mMessageTextView = findViewById(R.id.textView);
        mCountTextView = findViewById(R.id.textView2);
        Intent intent = getIntent();
        mMessageTextView.setText(R.string.toast_message);
        mCountTextView.setText(String.valueOf(intent.getIntExtra(MainActivity.EXTRA_COUNT, 0)));
    }
}
